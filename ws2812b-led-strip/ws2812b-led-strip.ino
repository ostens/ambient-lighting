#include <FastLED.h>

#define LED_PIN 7
#define NUM_LEDS 122
#define MAX_BRIGHTNESS 128
#define MIN_BRIGHTNESS 1
#define UPS 24
#define SPEED 8

CRGB leds[NUM_LEDS];

int colourIndex = 0;

int brightness = MIN_BRIGHTNESS;

int counter = SPEED;
    
void setup() {
  FastLED.addLeds<WS2812B, LED_PIN, RGB>(leds, NUM_LEDS);
}

void loop() {
  colourIndex += SPEED;
  if (brightness >= MAX_BRIGHTNESS) {
    counter = - SPEED;
  } else if (brightness <= MIN_BRIGHTNESS) {
    counter = SPEED;
  }
  brightness += counter;
  fill(colourIndex);
  FastLED.show();
  delay(1000/UPS);
}

void fill( int colorIndex) {   
    for(unsigned int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette(RainbowColors_p, colorIndex, brightness, LINEARBLEND);
        colorIndex += 255/NUM_LEDS;
    }
}
